package fonredis

import (
	"bitbucket.org/fonality/nf-commonUtils/recoverUtils"
	"errors"
	"fmt"
	"github.com/FZambia/go-sentinel"
	"github.com/garyburd/redigo/redis"
	json "github.com/json-iterator/go"
	"log"
	"time"
)

//a pool of redis connections
//redis client support common operations and connection pools
//to improve redis performance, pipeline command is also added
const WITH_SCORES = "WITHSCORES"

type RedisPool struct {
	pool       *redis.Pool
	server     string
	sentinels  []string
	masterName string
	pass       string
}

type PipelineDoArgs struct {
	Command    string
	ChanelName string
	Data       interface{}
}

type RedisClientIf interface {
	Close() error
	GetConnection() redis.Conn
	Do(command string, args ...interface{}) (ret interface{}, err error)
	GetMap(key string) (map[string]string, error)
	SetMapField(key, field string, value interface{}) error
	DeleteMapField(key string, field ...string) error
	GetMapStringField(key, field string) (string, error)
	DeleteKey(key string) error
	StoreMap(key string, m map[string]string) error
	GetSetMembers(key string) (ret []string, err error)

	GetStringVal(key string) (string, error)
	LPopListVal(key string) (string, error)

	//redis publish command only
	Publish(chName string, args ...interface{}) error

	//Pipe
	Pipeline(cmds []map[string][]interface{}) ([]interface{}, error)
	PipeLineDo(args []*PipelineDoArgs) error

	//check if key exists or not, true if exist false if not
	IsKeyExist(key string) bool
	GetCountsOfSet(key string) int64

	ZAdd(key string, values []interface{}) (int64, error)
	ZRange(key string, start, end int64, withscores bool) ([][]byte, error)

	//zrangebyscore redis command, score is int64 base
	//zremrangebyscore,remove elements in this set
	ZRangebyscore(key string, min int64, max int64) ([]string, error)
	ZRemrangebyscore(key string, min int64, max int64) error
}

//redis server
//maxIdle is the number of idle connections
//maxActive, is the maxium number of redis connections to the server
//redis server as default support around 511 tcp connnections
//pass, is the password used by redis to authencate the client and slaves
func NewRedisPoolWithSentinel(addrs []string, name string, maxIdle, maxActive int, pass string) *RedisPool {
	red := new(RedisPool)
	red.sentinels = addrs
	red.masterName = name
	red.pass = pass

	sntnl := &sentinel.Sentinel{
		Addrs:      red.sentinels,
		MasterName: red.masterName,
		Dial: func(addr string) (redis.Conn, error) {
			timeout := 50 * time.Second
			c, err := redis.DialTimeout("tcp", addr, timeout, timeout, timeout)
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}

	red.pool = &redis.Pool{
		MaxIdle:     maxIdle,
		MaxActive:   maxActive,
		Wait:        true,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			masterAddr, err := sntnl.MasterAddr()
			if err != nil {
				//retry once to avoid temporary unavailable case
				time.Sleep(time.Millisecond * 100)
				masterAddr, err = sntnl.MasterAddr()
			}

			if err != nil {
				return nil, err
			}

			log.Println("Redis master", masterAddr)
			op := redis.DialPassword(pass)
			c, err := redis.Dial("tcp", masterAddr, op)
			if err != nil {
				return nil, err
			}
			return c, nil
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if !sentinel.TestRole(c, "master") {
				return errors.New("Role check failed")
			} else {
				return nil
			}
		},
	}

	return red
}

//redis server
//maxIdle is the number of idle connections
//maxActive, is the maxium number of redis connections to the server
//redis server as default support around 511 tcp connnections
func NewRedisPool(redServer string, maxIdle int, maxActive int, pass string) *RedisPool {
	red := new(RedisPool)
	red.server = redServer
	red.pool = &redis.Pool{
		MaxIdle:   maxIdle,
		MaxActive: maxActive, //some operation system does not allow too many socket files
		Wait:      true,      //if false, redis operation will fail,have to wait
		Dial: func() (redis.Conn, error) {
			op := redis.DialPassword(pass)
			c, err := redis.Dial("tcp", redServer, op)
			if err != nil {
				return nil, err
			}
			return c, err
		},
	}

	return red
}

func (rp *RedisPool) Close() error {
	return rp.pool.Close()
}

func (this *RedisPool) GetConnection() redis.Conn {
	return this.pool.Get()
}

func (this *RedisPool) GetPubSubConnection() (*NFPubSub, error) {
	conn := this.pool.Get()

	err := conn.Err()
	if err != nil {
		return nil, err
	}

	nfPubsub := NFPubSub{Conn: &redis.PubSubConn{Conn: conn}}

	return &nfPubsub, nil
}

func (rp *RedisPool) Do(command string, args ...interface{}) (ret interface{}, err error) {
	con := rp.pool.Get()
	err = con.Err()

	if err != nil {
		time.Sleep(time.Millisecond * 100)
		con = rp.pool.Get()
		err = con.Err()
	}

	if err != nil {
		return
	}
	defer con.Close()
	return con.Do(command, args...)
}

func (rp *RedisPool) GetMap(key string) (map[string]string, error) {
	m, err := redis.StringMap(rp.Do("HGETALL", key))
	if err != nil || len(m) == 0 {
		return nil, errors.New("HGETALL return nothing")
	}
	return m, nil
}

func (rp *RedisPool) DeleteKey(key string) error {
	_, err := rp.Do("DEL", key)
	return err
}

func (rp *RedisPool) StoreMap(key string, m map[string]string) error {
	_, err := rp.Do("HMSET", redis.Args{}.Add(key).AddFlat(m)...)
	return err
}

func (rp *RedisPool) StoreMapGeneral(key string, m map[string]interface{}) error {
	_, err := rp.Do("HMSET", redis.Args{}.Add(key).AddFlat(m)...)
	return err
}

func (rp *RedisPool) GetSetMembers(key string) ([]string, error) {
	return redis.Strings(rp.Do("SMEMBERS", key))
}

func (rp *RedisPool) DeleteMapField(key string, fields ...string) error {
	options := make([]interface{}, 1+len(fields))
	options[0] = key
	for indx, field := range fields {
		options[indx+1] = field
	}
	_, err := rp.Do("HDEL", options...)
	return err
}

func (rp *RedisPool) Publish(chName string, args ...interface{}) error {
	_, err := rp.Do("PUBLISH", chName, fmt.Sprint(args...))
	return err
}

/*
EX seconds -- Set the specified expire time, in seconds.
PX milliseconds -- Set the specified expire time, in milliseconds.
NX -- Only set the key if it does not already exist.
XX -- Only set the key if it already exist.
*/
func (rp *RedisPool) SetString(key string, value string, ex int, px int, nx bool, xx bool) (string, error) {

	exist := ""
	if nx {
		exist = "NX"
	}

	if xx {
		exist = "XX"
	}

	var ret string
	var err error

	if ex > 0 {
		ret, err = redis.String(rp.Do("SET", key, value, "EX", ex, exist))
	} else if px > 0 {
		ret, err = redis.String(rp.Do("SET", key, value, "PX", ex, exist))
	} else {
		ret, err = redis.String(rp.Do("SET", key, value, exist))
	}

	if err == redis.ErrNil {
		return ret, nil
	} else {
		return ret, err
	}
}

func (rp *RedisPool) GetStringVal(key string) (string, error) {
	return redis.String(rp.Do("GET", key))
}

func (rp *RedisPool) LPopListVal(key string) (string, error) {
	return redis.String(rp.Do("LPOP", key))
}

func (rp *RedisPool) Pipeline(cmds []map[string][]interface{}) ([]interface{}, error) {
	var err error
	var res []interface{}

	con := rp.pool.Get()
	defer con.Close()

	count := 0
	for _, singleCmd := range cmds {
		//redis has some command with more than 4 args?
		for key, args := range singleCmd {
			switch len(args) {
			case 0:
				con.Send(key)
				count++
			case 1:
				con.Send(key, args[0])
				count++
			case 2:
				con.Send(key, args[0], args[1])
				count++
			case 3:
				con.Send(key, args[0], args[1], args[2])
				count++
			case 4:
				con.Send(key, args[0], args[1], args[2], args[3])
				count++
			default:
				log.Println("our pipeline command do not support 4 more args now!!")
			}
		}
	}

	err = con.Flush()
	if err != nil {
		return nil, err
	}

	for i := 0; i < count; i++ {
		r, err := con.Receive()
		if err != nil {
			log.Println("failed to get reply for pipeline")
			return nil, err
		}
		res = append(res, r)
	}
	return res, nil
}

func (this *RedisPool) PipeLineDo(args []*PipelineDoArgs) error {
	con := this.GetConnection()
	defer con.Close()

	for _, arg := range args {
		if arg != nil {
			log.Printf("sending to list %s event %v", arg.ChanelName, arg.Data)
			err := con.Send(arg.Command, arg.ChanelName, arg.Data)
			if err != nil {
				errMsg := fmt.Sprintf("Error in %s into %s due to %s", arg.Command, arg.ChanelName, err.Error())
				log.Printf(errMsg)
				return errors.New(errMsg)
			}
		}
	}
	return nil
}

func (rp *RedisPool) IsKeyExist(key string) bool {

	ret, err := rp.Do("EXISTS", key)

	if err != nil {
		return false
	}

	if ret.(int64) == 1 {
		return true
	}
	return false
}

//GetCountsOfSet return the numbers of the set
//zero if error happesn,otherwise postive count number
func (rp *RedisPool) GetCountsOfSet(key string) int64 {

	ret, err := rp.Do("SCARD", key)
	if err != nil {
		return 0
	}
	return ret.(int64)
}

func (this *RedisPool) ZAdd(key string, values []interface{}) (int64, error) {
	//fmt.Printf("%v\n", values)

	totalCount := len(values)
	c := make([]interface{}, 1+2*len(values))
	c[0] = key
	for i := 0; i < totalCount; i++ {
		indx := 2 * i
		jsonStr, err := json.Marshal(values[i])
		if err != nil {
			fmt.Printf("invalid object to convert to json due to %s", err.Error())
			continue
		}
		c[indx+1] = i
		c[indx+2] = string(jsonStr)
		//fmt.Println("score:", c[indx+1], ":", c[indx+2])
	}
	//for i :=0; i < 15; i++{
	//
	//	fmt.Println("score:", c[i])
	//	//i++
	//}
	//c = append(c, values...)
	result, err := this.Do("ZADD", c...)
	if err != nil {
		return 0, err
	}
	return result.(int64), nil
}

func (this *RedisPool) ZRange(key string, start, end int64, withscores bool) ([][]byte, error) {
	args := make([]interface{}, 3)
	args[0] = key
	args[1] = start
	args[2] = end
	if withscores {
		args = append(args, WITH_SCORES)
	}
	rets, err := redis.Values(this.Do("ZRANGE", args...))
	if err != nil {
		return nil, err
	}

	results := make([][]byte, len(rets))
	for j, item := range rets {
		val := item.([]uint8)
		b := make([]byte, len(val))
		for i, v := range val {
			b[i] = byte(v)
		}
		results[j] = b
	}
	return results, nil
}

func (this *RedisPool) SetMapField(key, field string, value interface{}) error {
	values := make([]interface{}, 3)
	values[0] = key
	values[1] = field
	values[2] = value
	_, err := this.Do("HSET", values...)
	//fmt.Printf("HSET type: %s", reflect.TypeOf(ret).String())
	return err
}

func (this *RedisPool) GetMapStringField(key, field string) (string, error) {
	args := make([]interface{}, 2)
	args[0] = key
	args[1] = field
	return redis.String(this.Do("HGET", args...))
}

//RPOPLPUSH source destination, pop the element and meanwhile push it into another list
func (rp *RedisPool) RPopLpush(src, dst string) (string, error) {
	return redis.String(rp.Do("RPOPLPUSH", src, dst))
}

func (rp *RedisPool) RPopListVal(key string) (string, error) {
	return redis.String(rp.Do("RPOP", key))
}

func (rp *RedisPool) ZRangebyscore(key string, min int64, max int64) ([]string, error) {
	if max == -1 {
		return redis.Strings(rp.Do("ZRANGEBYSCORE", key, min, "+inf"))
	} else {
		return redis.Strings(rp.Do("ZRANGEBYSCORE", key, min, max))
	}
}

func (rp *RedisPool) ZRemrangebyscore(key string, min int64, max int64) error {
	_, err := rp.Do("ZREMRANGEBYSCORE", key, min, max)
	return err
}

func (rp *RedisPool) GetRedisMaster() string {
	for _, address := range rp.sentinels {
		master := rp.tryRedisMaster(address)
		if master != "" {
			return master
		}
	}

	return ""
}

func (rp *RedisPool) tryRedisMaster(address string) string {
	defer recoverUtils.Recover()

	c, err := redis.Dial("tcp", address)
	if err != nil {
		log.Println("Redis sentinel listen connection error ", err.Error())
		return ""
	}

	res, err := redis.Strings(c.Do("sentinel", "get-master-addr-by-name", rp.masterName))
	if err != nil {
		log.Println("Redis get master error ", err.Error())
		return ""
	}
	newMaster := fmt.Sprintf("%s:%s", res[0], res[1])
	return newMaster
}

func (rp *RedisPool) GetSentinels() []string {
	return rp.sentinels
}

func (rp *RedisPool) GetMaster() string {
	return rp.masterName
}

func (rp *RedisPool) GetPass() string {
	return rp.pass
}

func (this *RedisPool) Get() redis.Conn {
	return this.pool.Get()
}

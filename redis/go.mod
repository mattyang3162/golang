module bitbucket.org/mattyang/golang/redis

replace bitbucket.org/fonality/nf-commonUtils => ../../nf-commonUtils

require (
	bitbucket.org/fonality/nf-commonData v0.0.0-20190807020455-f44869f8f474
	bitbucket.org/fonality/nf-commonUtils v0.0.0
	github.com/FZambia/go-sentinel v0.0.0-20171204085413-76bd05e8e22f
	github.com/garyburd/redigo v1.6.0
	github.com/json-iterator/go v1.1.5
	gopkg.in/redsync.v1 v1.0.1
)

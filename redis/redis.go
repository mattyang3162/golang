package redis

import (
	"bitbucket.org/fonality/nf-commonData/utils"
	"bitbucket.org/fonality/nf-commonUtils/fonredis"
	"log"
	"time"
)

const (
	ERR_WAIT_INVERVAL = 5
	SAME_ERR_LOG_MAX  = 10
)

type MsgProc struct {
	redisPool *fonredis.RedisPool
	listName  string
}

func StartMsgReceiver() {
	msgProc := &MsgProc{listName: "content:test"}
	msgProc.redisPool = utils.GetRedisConnectionPool()
	log.Print("Start receiving Redis message from list ", msgProc.listName)
	go msgProc.msgRev()
}

/*
2019/08/07 21:38:11 sandbox:content:test
2019/08/07 21:38:11 hello kdjfdkj jdkfjkdj
2019/08/07 21:38:41 Read content:test failure from redis server,  values len(0) err(read tcp 10.68.49.2:53776->10.64.36.98:6379: i/o timeout)
2019/08/07 21:39:16 Read content:test failure from redis server,  values len(0) err(read tcp 10.68.49.2:53801->10.64.36.98:6379: i/o timeout)

because the timeout is 30, the default is 10 seconds in radix code
return radix.Dial(network, addr,
			radix.DialTimeout(30*time.Second),
		)
*/
func (this *MsgProc) msgRev() {
	var errCnt int64

	r := this.redisPool
	for {
		// process will block waiting if redis queue is empty
		var values []string
		err := r.DoRet(&values, "BLPOP", this.listName, 0)
		if err != nil {
			//if err.Error() != "EOF" { //ignore false alarm of EOF
			if errCnt < SAME_ERR_LOG_MAX {
				log.Printf("Read %s failure from redis server,  values len(%d) err(%s)", this.listName, len(values), err)
				errCnt++
			}
			time.Sleep(ERR_WAIT_INVERVAL * time.Second)
			//}
			continue
		}
		if errCnt > 0 {
			log.Printf("Read %s from redis server recovered", this.listName)
			errCnt = 0
		}
		for _, v := range values {
			log.Println(v)
		}
	}
}

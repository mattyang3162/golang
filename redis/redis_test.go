package redis

import (
	"log"
	"os"
	"os/signal"
	"testing"
)

func TestRedisDistLock(t *testing.T) {
	t.Log("Test distributed lock ...")
	log.Print("Test DistLock ...")
	lock := NewLock()
	lock.Run()
}

/*
func TestRedsyncLock(t *testing.T) {
	t.Log("Test redsync ...")
	log.Print("Test redsync ...")
	lock := NewRedsyncLock()
	lock.Run()
}
*/

func TestMsgRecv(t *testing.T) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)

	log.Print("Test receive redis list ...")
	StartMsgReceiver()

	log.Println("Ctrl +C to end ... ")
	s := <-c
	log.Println("Receive signal:", s, " Stopping service now!")
}

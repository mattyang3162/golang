package redis

import (
	"bitbucket.org/fonality/nf-commonData/utils"
	//"bitbucket.org/fonality/nf-commonUtils/distlock"
	"bitbucket.org/fonality/nf-commonData/distlock"
	"gopkg.in/redsync.v1"
	"log"
	"time"
)

// DistLock ...
type DistLock struct {
	lock distlock.DistLockIf
}

type redsyncLock struct {
	lock *redsync.Redsync
}

// DistLockInf ...
type DistLockInf interface {
	Run()
}

func init() {
	utils.SetupConfig("contentService")
}

// NewLock ...
func NewLock() DistLockInf {
	l := &DistLock{}
	//redisPool := utils.GetRedisConnectionPool()
	//sentinels := redisPool.GetSentinels()
	//masterName := redisPool.GetMaster()
	//masterPass := redisPool.GetPass()
	duration := time.Duration(60) * time.Second
	retries := 10
	//maxActive := 2

	//	l.lock = distlock.NewDistLockClientWithSentinel(sentinels, masterName, duration, retries, "content:test:lock:", maxActive, masterPass)
	l.lock = distlock.NewDistLockClient(duration, retries, "content:test:lock:")
	return l
}

// Run test distributed lock
func (l *DistLock) Run() {
	c := make(chan int)
	max := 3
	for i := 0; i < max; i++ {
		go func(num int) {
			c <- l.getDistLock(num)
		}(i)
	}
	var cnt int
	for {
		num := <-c
		cnt = cnt + 1
		log.Print("Finished ", num)
		if cnt == max {
			break
		}
	}
}

func (l *DistLock) getDistLock(i int) int {
	log.Print("Start goroutine number ", i)
	log.Print("goroutine ", i, " asks for lock")
	ok, m := l.lock.Lock("mylock") // waiting for retries * 0.5 secs
	if ok == true && m != nil {
		log.Print("goroutine ", i, " gets lock")
		time.Sleep(time.Duration(5) * time.Second)
		isOk := l.lock.Unlock(m)
		log.Print("goroutine ", i, " releases for lock ", isOk)
	} else {
		log.Print("goroutine ", i, " can't get lock")
	}

	//	time.Sleep(time.Duration(5) * time.Second)
	return i
}

/*
// NewRedsyncLock returns native redsync
func NewRedsyncLock() DistLockInf {
	l := &redsyncLock{}
	pools := []redsync.Pool{}
	redisPool := utils.GetRedisConnectionPool()
	pools = append(pools, redisPool)

	rs := redsync.New(pools)
	l.lock = rs
	return l
}

func (l *redsyncLock) Run() {
	c := make(chan int)
	max := 3
	for i := 0; i < max; i++ {
		go func(num int) {
			c <- l.getDistLock(num)
		}(i)
	}
	var cnt int
	for {
		num := <-c
		cnt = cnt + 1
		log.Print("Finished ", num)
		if cnt == max {
			break
		}
	}
}

func (l *redsyncLock) getDistLock(i int) int {
	log.Print("Start goroutine number ", i)

	expiry := redsync.SetExpiry(60 * time.Second) // lock 60 seconds
	m := l.lock.NewMutex("content:test:lock:redsync", expiry)
	log.Print("goroutine ", i, " asks for lock")
	err := m.Lock()
	if err != nil {
		log.Print("goroutine ", i, " can't get lock err ", err.Error())
		return i
	}
	log.Print("goroutine ", i, " gets lock")
	time.Sleep(time.Duration(5) * time.Second)
	isOk := m.Unlock()
	log.Print("goroutine ", i, " releases for lock ", isOk)

	return i
}
*/
